var express = require('express');
const nodemailer = require('nodemailer');

let app = express();


app.use('/',function(req,res,next)
{   res.setHeader('x-powered-by','Just Email ME!');
    res.setHeader("X-Frame-Options", "DENY");
    res.setHeader("Content-Security-Policy", "frame-ancestors 'none'");    
    next();
});
app.get('/',function(req, res){

    res.write('<h1> Hello World! </h1>');
    res.end();
});

app.get('/send',function(req, res){
    console.log('test');
   nodemailer.createTestAccount((err, account) => {

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'youremail@gmail.com', // generated ethereal user
                pass: 'yourpassword'  // generated ethereal password
            }
        });
        
        let mailOptions = {
            to: 'toemail@gmail.com', // list of receivers
            subject: 'Hello ✔', // Subject line
            text: 'Hello world?', // plain text body
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                res.json({'error':error});
            }else{

            res.json(info);
            }
            transporter.close();
            // console.log('Message sent: %s', info.messageId);
            // // Preview only available when sending through an Ethereal account
            // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@blurdybloop.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    });

});
app.listen(4444);